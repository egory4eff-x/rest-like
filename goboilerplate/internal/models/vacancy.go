package models

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    vacancy, err := UnmarshalVacancy(bytes)
//    bytes, err = vacancy.Marshal()

import "encoding/json"

func UnmarshalVacancy(data []byte) (Vacancy, error) {
	var r Vacancy
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Vacancy) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Vacancy struct {
	Identifier     `json:"identifier" bson:"identifier"`
	DatePosted     string `json:"datePosted" db:"date_posted" bson:"date_posted"`
	Title          string `json:"title" db:"title" bson:"title"`
	Description    string `json:"description" db:"description" bson:"description"`
	ValidThrough   string `json:"validThrough" db:"valid_through" bson:"valid_through"`
	EmploymentType string `json:"employmentType" db:"employment_type" bson:"employment_type"`
}

type Identifier struct {
	Value string `json:"value" db:"vacancy_id" bson:"vacancy_id"`
	Name  string `json:"name" db:"organization" bson:"organization"`
}
